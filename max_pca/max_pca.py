"""
Implementation of ideas about max-stable PCA using a scikit-learn like interface for applications.

The following utilites are provided:

    - Transformation of the data to unit Frechet margins and retransforming the data to the original margins using the 
    functions trafo_frech1 to transform to standardized margins and trafo_orig to transform to the original marginal transformations.
    This is done using the empirical distribution function, however for some applications it might be more adequate to use ML estimates 
    and pseudoinverses of the ML estimate. This should be considered by the user.
    
    - An empirical estimator of the stable tail dependence valid in all dimensions. The estimator is created via a constructor with the data, 
    but can be called like a function for convenience of use. However for the use of the PCA proceedure it is usually not explicitly needed.

    - A class that implements max-stable PCA, following the idea of instanciate an object, fit to the data via a fit method, then get summaries 
    or transform the data usign the corresponding methods.
"""

import pandas as pd
import numpy as np
from scipy.optimize import minimize

class maxPrincipalComponentAnalysis(object):
    """
    class to use and store data about max-pca. 

    methods:
        __init__    : initialize object
        fit         : fit a max-stable pca to the data given as argument
        summary     : get information on the convergence of the optimizer and the value of the metric after completion
        transform   : transform a dataset using max-stable pca.
    """

    def __init__(self, full=False):
        """
        Initialize the object, all parameters should be set using the fit function.
        """
        if full:
            self._create_H = _create_H_full
            self.fitmode = "Full"
        else:
            self._create_H = _create_H
            self.fitmode = "Sparse"

        self.H = None
        self.W = None
        self.data = None
        self.optimizer_results = None

    def fit(self, data, p, k, simplify_to_convex = False, optim_options={'maxiter' : 500}):
        """
        Fit a max-stable pca to the data and saves it in the attributes of the object that is, finding a max-linear map that represents the data well with respect to the the empirical stable tail dependence
        of the data. 

        params:
            data: arraylike of shape (n, d), where the rows are representing the data and the columns are the entries of the multivariate 
            unit frechet data. If the data is not guaranteed to have Frechet-1 margins consider unsing the function trafo_frech1 on the data array
            to ensure the correct margins. This does affect performance of the algorithm.
            p: int greater than 0. The number of principal components/ dimension of the subspace. The first p columns of data are used to create the reconstruction
            so consider permuting columns to use columns that are as independent as possible. 
            k: int greater than 0. Tuning parameter for the stable tail dependence estimator. See StableTailDependenceEstimator for some more info.
            simplify_to_convex: TODO

        returns:
            None
        """
        self.data = data
        est_std = StableTailDependenceEstimator(data, k) 

        def target_fn(H, W):
            """
            Target function of the minimization problem for the current set of columns H and the reconstructions done by W.

            params:
                H: arraylike of shape (d, p) where p < d
                W: arraylike of shape (p, d-p)

            returns:
                float, representing the current loss the reconstruction has w.r.t. the given data in the estimator
            """
            Hmat = self._create_H(H, W)
            # H_vee_id = np.maximum(Hmat, np.eye(Hmat.shape[0]))
            return np.sum(np.apply_along_axis(est_std, 1, Hmat))
        
        def target_fn_non_conv(H, W):
            """
            Target function of the minimization problem for the current set of columns H and the reconstructions done by W.

            params:
                H: arraylike of shape (d, p) where p < d
                W: arraylike of shape (p, d-p)

            returns:
                float, representing the current loss the reconstruction has w.r.t. the given data in the estimator
            """
            Hmat = self._create_H(H, W)
            H_vee_id = np.maximum(Hmat, np.eye(Hmat.shape[0]))
            return 2 * np.sum(np.apply_along_axis(est_std, 1, H_vee_id)) - np.sum(np.apply_along_axis(est_std, 1, Hmat))

        def constraint_fn_lower(H, W):
            """
            params:
                H: arraylike of shape (d, p) where p < d
                W: arraylike of shape (p, d-p)

            returns:
                np.array of shape (d,), representing the deviation from the constraint
            """
            Hmat = self._create_H(H, W)
            return np.sum(Hmat, axis=1) - 1

        def constraint_fn_upper(H, W):
            """
            params:
                H: arraylike of shape (d, p) where p < d
                W: arraylike of shape (p, d-p)

            returns:
                np.array of shape (d,), representing the deviation from the constraint
            """
            Hmat = self._create_H(H, W)
            return data.shape[1] - np.sum(Hmat, axis=1)



        def constraint_ineq_fn(H, W):
            """
            helper function to ensure that the parameters are greater or equal to zero.

            params:
                H: arraylike of shape (d, p) where p < d
                W: arraylike of shape (p, d-p)

            returns:
                np.array of shape(d**2, ) the flattened H matrix.
            """
            return self._create_H(H,W).flatten()

        # get dimensions from the data and reconstruction factorization
        # TODO: setting the parameter in the functions here is messy but works and saves function calls
        d = data.shape[1]
        if self.fitmode == "Full":
            ncols_W = d
        elif self.fitmode == "Sparse":
            ncols_W = d - p
        else: 
            raise ValueError("Parameter for fitmode is invalid and it should be avoided to manually change it")

        def target(x):
            H = x[:(p*d)].reshape((d,p))
            W = x[(p*d):].reshape((p,ncols_W))
            return target_fn(H,W)

        def target_non_conv(x):
            H = x[:(p*d)].reshape((d,p))
            W = x[(p*d):].reshape((p,ncols_W))
            return target_fn_non_conv(H,W)

        def constr1(x):
            H = x[:(p*d)].reshape((d,p))
            W = x[(p*d):].reshape((p,ncols_W))
            return constraint_ineq_fn(H, W)

        def constr_upper(x):
            H = x[:(p*d)].reshape((d,p))
            W = x[(p*d):].reshape((p,ncols_W))
            return constraint_fn_upper(H, W)

        def constr_lower(x):
            H = x[:(p*d)].reshape((d,p))
            W = x[(p*d):].reshape((p,ncols_W))
            return constraint_fn_lower(H, W)

        def get_HW(x, p, d):
            H = x[0:(p*d)].reshape((d,p))
            W = x[(p*d):].reshape((p,ncols_W))
            return H, W


        if self.fitmode == "Sparse":
            xinit = np.zeros(d*p + p * ncols_W)
        elif self.fitmode == "Full":
            print("set up full fit intial values")
            xinit = np.zeros(2 * d * p)

        for i in range(p):
            xinit[i*d + i] = 1

        if self.fitmode == "Full":
            xinit[(p * d):] = 1

        # set up minimizer syntax
        b = (0, 100)
        bnds = [b for _ in xinit]

        const1 = {'type':'ineq', 'fun': constr_upper}
        const2 = {'type':'ineq', 'fun': constr_lower}
        cons = [const1, const2]

        # optimize using quadratic programming
        if simplify_to_convex:
            self.optimizer_results = minimize(target, xinit, method='SLSQP', bounds=bnds, constraints=cons, options=optim_options)
            self.H, self.W = get_HW(self.optimizer_results.x, p, d)
            print(f'optimizer finished with status {self.optimizer_results.message}')
        else:
            self.optimizer_results = minimize(target_non_conv, xinit, method='SLSQP', bounds=bnds, constraints=cons, options=optim_options)
            self.H, self.W = get_HW(self.optimizer_results.x, p, d)
            print(f'optimizer finished with status {self.optimizer_results.message}')


    def transform(self, x):
        """
        Transforms the data given with the PCA reconstruction matrix

        params:
            x: arraylike of shape (n, d) where d is the number of columns of the original data and n can be any integer
        """
        Hmat = self._create_H(self.H, self.W)
        return np.apply_along_axis(lambda z: np.max(Hmat * z, axis = 1), 1, x)

    def summary(self):
        """
        Gives some summary statistics on the optimizer and the calue of the target function. 
        Care is needed if the sum of metric over marginals is large or the optimizer did not succeed.
        """
        return {
                'sum of target function over marginals' : self.optimizer_results.fun,
                'sum of metric over marginals' : self.optimizer_results.fun - self.data.shape[1],
                'optimizer success' : self.optimizer_results.success,
                'optimizer message' : self.optimizer_results.message,
                'fit mode' : self.fitmode,
                }


class StableTailDependenceEstimator(object):
    """
    Class for the estimation of the stable tail dependence, 

    methods:
        __init__    : initalize object using data and a given hyperparameter k
        __call__    : evaluate the stable tail dependence estimator on an entry
    """

    def __init__(self, data, k):
        """
        data: arraylike with dimension n x d, where the rows are observations, the columns are the entries of
        the multivariate random law
        k: int greater than zero, should be from a intermediary sequence
        """
        self.data = data
        self.norms = np.sum(np.abs(data), axis=1)
        self.k = k
        self.n = self.data.shape[0]
        self.valid_indices = self.norms > (self.n / self.k)

    def __call__(self, x):
        """
        evaluate the estimator and make the object useable as a function.

        params:
            x: arraylike of dimension (d,)

        returns:
            float, the estimator for the stable tail dependence evaluated at x
        """
        empirical_filtered = np.max(self.data * x, axis = 1)[self.valid_indices] / self.norms[self.valid_indices]
        result = np.sum(empirical_filtered) / self.k
        return result

def trafo_frech1(data, intercept = 0.01, shrink = 0.99):
    """
    Transform a dataset to (approximately) unit Fréchet amrgins using the formaula from Resnick 1987. 
    We apply a transformation of the form
        
        data_to_transform = shrink * data + intercept

    to avoid division by zero.

    params:
        data: arraylike of shape (n, d), dataset to be transformed to unit Frechet margins
        intercept: float, should be small 
        shrink: float, value should be between zero and 1. Using values greater or equal to one will cause divisions by zero thus errors or unwanted behaviour.

    returns:
        arraylike of shape (n, d) that is the transformation of data to approximately Frechet margins.
    """
    result = np.zeros(shape=data.shape).T

    for i, col in enumerate(data.T):
        def ecdf_col(x):
            return _emp_ecdf(x, col)

        result[i,:] = np.array([-1 / np.log(ecdf_col(shrink * x + intercept)) for x in col])

    return result.T

def trafo_orig(data_orig, data_trafo):
    """
    transform the dataset data_trafo with unit Frechet margins to approximately the same margins as data_orig.

    params:
        data_orig: arraylike of shape (n, d)
        data_trafo: arraylike of shape (n, d), should have approximately unit fréchet margins for this function to make sense.

    returns:
        arraylike of shape (n,d) that is the transformed version of data_trafo to the margins of data_orig
    """
    result = []
    for j in range(data_orig.shape[1]):
        col = []
        for val in data_trafo[:,j]:
            print(val)
            col.append(_emp_pseudoinverse(np.exp(-1 / val), data_orig[:,j]))

        result.append(col)
    
    result = np.array(result).T
    return result

def _emp_ecdf(x, realiz):
    """
    empirical distribution function of the data realiz.

    params:
        x: float, the point where the ecdf is evaluated
        realiz: arraylike of shape (n,) a realization of some data.ö

    returns:
        float between 0 and 1, representing the percentage of realizations smaller or equal to x.
    """
    return len(realiz[realiz <= x]) / len(realiz)

def _emp_pseudoinverse(u, data):
    """
    Empirical pseudoinverse function using order statistics.

    params:
        data: array of shape (n,) data for which the pseudoinverse is wanted
        u float between 0 and 1, the point at which the pseudoinverse is to be evaluated

    returns:
        float, the value of the data which empirical distribution function is the smalles value greater or equal than u.
    """
    data_sorted = np.sort(data)
    n = len(data)
    for i, val in enumerate(data_sorted):
        if (i + 1) / n >= u:
            return val

    return Exception
    
def _create_H_full(B, V):
    """
    creates a max-matrix for the inputs where the first p columns are H, the remaining (d-p) columns
    are max linear combinations-

    params:
        B: arraylike of shape (d, p) where p < d
        V: arraylike of shape (p, d)

    returns:
        B * V as a max matrix multiplication
    """
    Hmat = np.empty(shape=(B.shape[0], B.shape[0]))
    for j in range(Hmat.shape[1]):
        for i in range(Hmat.shape[1]):
            Hmat[i,j] = np.max(B[i,:] * V[:,j])

    return Hmat

def _create_H(H, W):
    """
    creates a max-matrix for the inputs where the first p columns are H, the remaining (d-p) columns
    are max linear combinations-

    params:
        H: arraylike of shape (d, p) where p < d
        W: arraylike of shape (p, d-p)

    returns:
        H * (id_p, W) as a max matrix multiplication
    """
    Hmat = np.empty(shape=(H.shape[0], H.shape[0]))
    Hmat[:,:H.shape[1]] = H
    for j in range(H.shape[0] - H.shape[1]):
        for i in range(H.shape[0]):
            Hmat[i,H.shape[1] + j] = np.max(H[i,:] * W[:,j])

    return Hmat
